This project hosts styles that provide a "Dark Mode" viewing experience for various websites.

## Requirements

The [Stylus](https://github.com/openstyles/stylus) browser extension.

## Installation

Click one of these buttons and you'll be taken to the install page for that site's style.

[![](https://img.shields.io/badge/Rolling%20Stone%20-Install-00adad.svg)](https://gitlab.com/awebeer256/dark-modes/-/raw/master/dark-rolling-stone.user.css)